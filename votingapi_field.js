
// crap!
var field;

function votingapi_field_vote(fieldname, nid, votefields) {

  var a = votefields.split("/");
  for (var x in a) {

    x = a[x]; if (!x) continue;
  
    // build the URL
    var url = '?q=votingapi_field/ajax/';
    url += fieldname;
    url += '/';
    url += nid;
    url += '/';
    url += $("#"+x+" div #edit-vote").val();
    field = $("#"+x+" div"); // this breaks the loop :-(

    // vote-in-place
    $.ajax({
      type: "GET",
      url: url,
      complete: function(xreq, msg){
        field.get(0).innerHTML = xreq.responseText;
        field.children("input").remove();
      }
    });
  }

  // don't page-refresh
  return false;
}
